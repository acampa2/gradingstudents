import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Main {

    /*
     * Complete the gradingStudents function below.
     */
    static int[] gradingStudents(int[] grades) {
        /*
         * Write your code here.
         */
        int[] results =new int [grades.length];

        for(int i=0;i<grades.length;i++){
            if(grades[i]<38){
                results[i]=grades[i];
            }
            else if(grades[i]%5==0){
                results[i]=grades[i];
            }
            else if (5-(grades[i])%5>3){
                results[i]=grades[i];
            }
            else if(5-(grades[i]%5) <3) {
                results[i] = grades[i] + 5-(grades[i]%5);
            }
            else if(5-(grades[i]%5) ==3){
                results[i]=grades[i];
            }
        }

        return results;

    }


    public static void main(String[] args) throws IOException {


        int[] grades = {80,96,18,73,78};

        int[] result = gradingStudents(grades);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            System.out.println(result[resultItr]);
        }

    }



}
